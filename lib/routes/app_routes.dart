part of 'app_pages.dart';

abstract class Routes {
  static const Splash = '/splash';
  static const LoginRegister = '/loginRegister';
  static const Home = '/home';
}