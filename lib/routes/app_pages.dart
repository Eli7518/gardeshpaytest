import 'package:flutter_gardesh_pay_test/bindings/splash_bindings.dart';
import 'package:flutter_gardesh_pay_test/presentation/views/Home/home.dart';
import 'package:flutter_gardesh_pay_test/presentation/views/LoginRegister/login_register.dart';
import 'package:flutter_gardesh_pay_test/presentation/views/Splash/splash.dart';
import 'package:get/get.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.Splash;

  static final routes = [
    GetPage(
      name: Routes.Splash,
      binding: InitBinding(),
      page: () => SplashView(),
    ),
    GetPage(
      name: Routes.LoginRegister,
      binding: InitBinding(),
      page: () => LoginRegisterView(),
    ),
    GetPage(
      name: Routes.Home,
      binding: InitBinding(),
      page: () => HomeView(),
    ),
  ];
}
