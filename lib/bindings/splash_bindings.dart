import 'package:flutter_gardesh_pay_test/data/login_register_api_provider.dart';
import 'package:flutter_gardesh_pay_test/data/login_register_repository.dart';
import 'package:flutter_gardesh_pay_test/data/splash_api_provider.dart';
import 'package:flutter_gardesh_pay_test/data/splash_repository.dart';
import 'package:flutter_gardesh_pay_test/domain/adapters/login_register_repository_adapter.dart';
import 'package:flutter_gardesh_pay_test/domain/adapters/splash_repository_adapter.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/home_controller.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/login_register_controller.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/splash_controller.dart';
import 'package:get/get.dart';

class InitBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ISplashProvider>(() => SplashProvider());
    Get.lazyPut<ISplashRepository>(
        () => SplashRepository(provider: Get.find()));
    Get.put<SplashController>(SplashController(splashRepository: Get.find()));
    Get.lazyPut<ILoginRegisterProvider>(() => LoginRegisterProvider());
    Get.lazyPut<ILoginRegisterRepository>(
        () => LoginRegisterRepository(provider: Get.find()));
    Get.lazyPut<LoginRegisterController>(
        () => LoginRegisterController(loginRegisterRepository: Get.find()));
    Get.lazyPut<HomeController>(() => HomeController());
  }
}
