import 'package:flutter_gardesh_pay_test/domain/adapters/login_register_repository_adapter.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/email_otp_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/login_register_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';
import 'login_register_api_provider.dart';

class LoginRegisterRepository implements ILoginRegisterRepository {
  LoginRegisterRepository({required this.provider});

  final ILoginRegisterProvider provider;

  @override
  Future<RegisterApiRes> loginUser(
      String deviceId, LoginRegisterReq req) async {
    final response =
        await provider.loginUser("/user/auth/login/email", deviceId, req);
    if (response.status.hasError) {
      return Future.error(response.statusText!);
    } else {
      return response.body!;
    }
  }

  @override
  Future<RegisterApiRes> registerUser(
      String deviceId, LoginRegisterReq req) async {
    final response =
        await provider.registerUser("/user/auth/register/email", deviceId, req);
    if (response.status.hasError) {
      return Future.error(response.statusText!);
    } else {
      return response.body!;
    }
  }

  @override
  Future<RegisterApiRes> verifyCode(String deviceId, VerifyCodeReq req) async {
    final response =
        await provider.verifyCode("/user/auth/register/email", deviceId, req);
    if (response.status.hasError) {
      return Future.error(response.statusText!);
    } else {
      return response.body!;
    }
  }
}
