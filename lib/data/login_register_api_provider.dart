import 'package:flutter_gardesh_pay_test/domain/entity/request/email_otp_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/login_register_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';
import 'package:get/get.dart';

abstract class ILoginRegisterProvider {
  Future<Response<RegisterApiRes>> loginUser(
      String path, String deviceId, LoginRegisterReq req);

  Future<Response<RegisterApiRes>> registerUser(
      String path, String deviceId, LoginRegisterReq req);

  Future<Response<RegisterApiRes>> verifyCode(
      String path, String deviceId, VerifyCodeReq req);
}

class LoginRegisterProvider extends GetConnect
    implements ILoginRegisterProvider {
  @override
  void onInit() {
    httpClient.defaultDecoder =
        (val) => RegisterApiRes.fromJson(val as Map<String, dynamic>);
    httpClient.baseUrl = 'https://cook-mook.herokuapp.com/v1';
  }

  @override
  Future<Response<RegisterApiRes>> loginUser(
      String path, String deviceId, LoginRegisterReq req) {
    var parts = [];
    req.toJson().forEach((key, value) {
      parts.add('${Uri.encodeQueryComponent(key)}='
          '${Uri.encodeQueryComponent(value)}');
    });
    var formData = parts.join('&');
    return post<RegisterApiRes>(
      path,
      formData,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "device-id": "$deviceId"
      },
    );
  }

  @override
  Future<Response<RegisterApiRes>> registerUser(
      String path, String deviceId, LoginRegisterReq req) {
    var parts = [];
    req.toJson().forEach((key, value) {
      parts.add('${Uri.encodeQueryComponent(key)}='
          '${Uri.encodeQueryComponent(value)}');
    });
    var formData = parts.join('&');
    return post<RegisterApiRes>(
      path,
      formData,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "device-id": "$deviceId"
      },
    );
  }

  @override
  Future<Response<RegisterApiRes>> verifyCode(
      String path, String deviceId, VerifyCodeReq req) {
    var parts = [];
    req.toJson().forEach((key, value) {
      parts.add('${Uri.encodeQueryComponent(key)}='
          '${Uri.encodeQueryComponent(value)}');
    });
    var formData = parts.join('&');
    return httpClient.request<RegisterApiRes>(
      path,
      "GET",
      body: formData,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "device-id": "$deviceId"
      },
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    );
  }
}
