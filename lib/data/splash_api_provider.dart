import 'package:flutter_gardesh_pay_test/domain/entity/request/register_device_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';
import 'package:get/get.dart';

abstract class ISplashProvider {
  Future<Response<RegisterApiRes>> registerDevice(
      String path, String deviceId, RegisterDeviceReq req);
}

class SplashProvider extends GetConnect implements ISplashProvider {
  @override
  void onInit() {
    httpClient.defaultDecoder =
        (val) => RegisterApiRes.fromJson(val as Map<String, dynamic>);
    httpClient.baseUrl = 'https://cook-mook.herokuapp.com/v1';
  }

  @override
  Future<Response<RegisterApiRes>> registerDevice(
      String path, String deviceId, RegisterDeviceReq req) {
    var parts = [];
    req.toJson().forEach((key, value) {
      parts.add('${Uri.encodeQueryComponent(key)}='
          '${Uri.encodeQueryComponent(value)}');
    });
    var formData = parts.join('&');
    return post<RegisterApiRes>(
      path,
      formData,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "device-id": "$deviceId"
      },
    );
  }
}
