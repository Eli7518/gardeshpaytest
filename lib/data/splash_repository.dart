import 'package:flutter_gardesh_pay_test/data/splash_api_provider.dart';
import 'package:flutter_gardesh_pay_test/domain/adapters/splash_repository_adapter.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';

class SplashRepository implements ISplashRepository {
  SplashRepository({required this.provider});

  final ISplashProvider provider;

  @override
  Future<RegisterApiRes> registerDevice(String deviceId, request) async {
    final response =
        await provider.registerDevice("/device/register", deviceId, request);
    if (response.status.hasError) {
      return Future.error(response.statusText!);
    } else {
      return response.body!;
    }
  }
}
