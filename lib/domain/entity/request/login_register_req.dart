class LoginRegisterReq {
  final String email;
  final String password;

  LoginRegisterReq({
    required this.email,
    required this.password,
  });

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
