class VerifyCodeReq {
  final String email;
  final String otpCode;

  VerifyCodeReq({
    required this.email,
    required this.otpCode,
  });

  Map<String, dynamic> toJson() => {
        "email": email,
        "otp_code": otpCode,
      };
}
