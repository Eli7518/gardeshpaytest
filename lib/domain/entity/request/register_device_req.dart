class RegisterDeviceReq {
  final String platformType;
  final String appStoreType;
  final String appVersion;
  final String osVersion;
  final String fcmToken;
  final String apnsToken;
  final String deviceModel;
  final String deviceManufacture;

  RegisterDeviceReq({
    required this.platformType,
    required this.appStoreType,
    required this.appVersion,
    required this.osVersion,
    required this.fcmToken,
    required this.apnsToken,
    required this.deviceModel,
    required this.deviceManufacture,
  });

  Map<String, dynamic> toJson() => {
        "platform_type": platformType,
        "app_store_type": appStoreType,
        "app_version": appVersion,
        "os_version": osVersion,
        "fcm_token": fcmToken,
        "apns_token": apnsToken,
        "device_model": deviceModel,
        "device_manufacture": deviceManufacture,
      };
}
