import 'dart:convert';
import 'result.dart';
import 'status.dart';

class RegisterApiRes {
  final Result? result;
  final Status status;

  RegisterApiRes({
    required this.result,
    required this.status,
  });

  factory RegisterApiRes.fromRawJson(String str) =>
      RegisterApiRes.fromJson(json.decode(str) as Map<String, dynamic>);

  factory RegisterApiRes.fromJson(Map<String, dynamic> json) => RegisterApiRes(
        result: (json['result'] != null)
            ? Result.fromJson(json["result"] as Map<String, dynamic>)
            : null,
        status: Status.fromJson(json["status"] as Map<String, dynamic>),
      );
}
