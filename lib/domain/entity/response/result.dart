import 'dart:convert';

import 'package:flutter_gardesh_pay_test/domain/entity/response/data.dart';

class Result {
  final Data? data;
  final String message;

  Result({
    required this.data,
    required this.message,
  });

  factory Result.fromRawJson(String str) =>
      Result.fromJson(json.decode(str) as Map<String, dynamic>);

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        data: (json["data"] != null && json["data"] != "")
            ? Data.fromJson(json["data"])
            : null,
        message: json["message"] ?? "",
      );
}
