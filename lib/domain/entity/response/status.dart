import 'dart:convert';

class Status {
  final int code;
  final bool isSuccess;
  final String message;

  Status({
    required this.code,
    required this.isSuccess,
    required this.message,
  });

  factory Status.fromRawJson(String str) =>
      Status.fromJson(json.decode(str) as Map<String, dynamic>);

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        code: json["code"] as int,
        isSuccess: json["is_success"] as bool,
        message: json["message"] as String,
      );
}
