import 'dart:convert';

class Data {
  final String accessToken;
  final String refreshToken;

  Data({
    required this.accessToken,
    required this.refreshToken,
  });

  factory Data.fromRawJson(String str) =>
      Data.fromJson(json.decode(str) as Map<String, dynamic>);

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accessToken: json["access_token"] ?? "",
        refreshToken: json["refresh_token"] ?? "",
      );
}
