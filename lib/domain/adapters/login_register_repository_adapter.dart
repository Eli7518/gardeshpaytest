import 'package:flutter_gardesh_pay_test/domain/entity/request/email_otp_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/login_register_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';

abstract class ILoginRegisterRepository {
  Future<RegisterApiRes> loginUser(String deviceId, LoginRegisterReq req);

  Future<RegisterApiRes> registerUser(String deviceId, LoginRegisterReq req);

  Future<RegisterApiRes> verifyCode(String deviceId, VerifyCodeReq req);
}
