import 'package:flutter_gardesh_pay_test/domain/entity/request/register_device_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';

abstract class ISplashRepository {
  Future<RegisterApiRes> registerDevice(String deviceId, RegisterDeviceReq req);
}
