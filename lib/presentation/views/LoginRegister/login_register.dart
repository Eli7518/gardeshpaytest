import 'package:flutter/material.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/login_register_controller.dart';
import 'package:get/get.dart';
import 'pages/login_page.dart';
import 'pages/register_page.dart';
import 'pages/login_register_page.dart';
import 'pages/verify_code_page.dart';

class LoginRegisterView extends GetView<LoginRegisterController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: controller.onWillPop,
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 30.0),
          child: PageView(
            controller: controller.pageViewController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              LoginRegisterPage(),
              LoginPage(),
              RegisterPage(),
              VerifyCodePage(),
            ],
          ),
        ),
      ),
    );
  }
}
