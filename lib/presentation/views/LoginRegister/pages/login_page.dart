import 'package:flutter/material.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/login_register_controller.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<LoginRegisterController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Image.network(
          'https://s.anardoni.com/applications/2021/03/02/photos/yF0s1kwml/icon_yF0s1kwml_2021_03_02_15_41_52.jpeg',
          width: Get.width / 4,
          height: Get.width / 4,
          fit: BoxFit.fill,
        ),
        Form(
          key: controller.loginFormKey,
          child: Column(
            children: [
              TextFormField(
                controller: controller.emailLoginController,
                validator: controller.emailTextFieldValidator,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email address as abc@gmail.com'),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: controller.passwordLoginController,
                validator: controller.passwordTextFieldValidator,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
              ),
              SizedBox(
                height: 15,
              ),
              Obx(
                () => TextButton(
                  onPressed: controller.loginIsLoading.isTrue
                      ? () {}
                      : controller.sendLoginInfo,
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    backgroundColor: Colors.blue,
                    fixedSize: Size(
                      Get.width / 2,
                      50,
                    ),
                  ),
                  child: Text(
                    controller.loginIsLoading.isTrue ? 'Loading...' : 'Login',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
