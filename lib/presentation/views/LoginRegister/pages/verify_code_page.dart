import 'package:flutter/material.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/login_register_controller.dart';
import 'package:get/get.dart';

class VerifyCodePage extends GetView<LoginRegisterController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Image.network(
          'https://s.anardoni.com/applications/2021/03/02/photos/yF0s1kwml/icon_yF0s1kwml_2021_03_02_15_41_52.jpeg',
          width: Get.width / 4,
          height: Get.width / 4,
          fit: BoxFit.fill,
        ),
        Form(
          key: controller.verifyFormKey,
          child: Column(
            children: [
              TextFormField(
                controller: controller.verifyCodeController,
                validator: controller.passwordTextFieldValidator,
                obscureText: true,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Code',
                    hintText: 'Enter the code that sent to your email'),
              ),
              SizedBox(
                height: 15,
              ),
              Obx(
                () => TextButton(
                  onPressed: controller.verifyIsLoading.isTrue
                      ? () {}
                      : controller.sendVerifyCode,
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    backgroundColor: Colors.blue,
                    fixedSize: Size(
                      Get.width / 2,
                      50,
                    ),
                  ),
                  child: Text(
                    controller.verifyIsLoading.isTrue ? 'Loading...' : 'Verify',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
