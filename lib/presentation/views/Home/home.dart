import 'package:flutter/material.dart';
import 'package:flutter_gardesh_pay_test/presentation/controllers/home_controller.dart';
import 'package:get/get.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(child: Text('Device ID:')),
                Flexible(child: Text(controller.deviceId)),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(child: Text('User Email:')),
                Flexible(child: Text(controller.userEmail)),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(child: Text('Count:')),
                Flexible(child: Text(controller.count.toString())),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(child: Text('Access Token:')),
                Flexible(child: Text(controller.accessToken)),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(child: Text('Refresh Token:')),
                Flexible(child: Text(controller.refreshToken)),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            TextButton(
              onPressed: controller.logout,
              style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                backgroundColor: Colors.blue,
                fixedSize: Size(
                  Get.width / 2,
                  50,
                ),
              ),
              child: Text(
                'Logout',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
