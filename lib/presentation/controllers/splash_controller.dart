import 'package:device_info/device_info.dart';
import 'package:flutter_gardesh_pay_test/domain/adapters/splash_repository_adapter.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/register_device_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:package_info/package_info.dart';
import 'package:uuid/uuid.dart';

class SplashController extends SuperController<RegisterApiRes> {
  SplashController({required this.splashRepository});

  final ISplashRepository splashRepository;
  final storageBox = GetStorage();

  bool deviceIdIsExists() {
    if (storageBox.hasData('device_id'))
      return true;
    else
      return false;
  }

  bool isUserLoggedIn() {
    if (storageBox.hasData('access_token') &&
        storageBox.hasData('refresh_token'))
      return true;
    else
      return false;
  }

  @override
  void onInit() async {
    super.onInit();
    print(storageBox.read('device_id'));
    String uuidRandom = "";
    if (!deviceIdIsExists()) {
      uuidRandom = Uuid().v4();
      await registerDeviceInfo(uuidRandom);
    } else {
      await registerDeviceInfo(storageBox.read('device_id'));
      await Future.delayed(
        Duration(seconds: 3),
      );
    }
    if (!isUserLoggedIn())
      Get.rootDelegate.offNamed('/loginRegister');
    else
      Get.rootDelegate.offNamed('/home');
  }

  Future<void> registerDeviceInfo(String deviceId) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (GetPlatform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      await splashRepository
          .registerDevice(
        deviceId,
        RegisterDeviceReq(
          platformType: 'Android',
          appStoreType: 'google_play',
          appVersion: packageInfo.version,
          osVersion: androidDeviceInfo.version.codename,
          fcmToken: '',
          apnsToken: '',
          deviceModel: androidDeviceInfo.model,
          deviceManufacture: androidDeviceInfo.manufacturer,
        ),
      )
          .then((value) {
        if (!deviceIdIsExists()) {
          storageBox.write('device_id', deviceId);
        }
      });
    } else if (GetPlatform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
      await splashRepository
          .registerDevice(
        deviceId,
        RegisterDeviceReq(
          platformType: 'iOS',
          appStoreType: 'apple_app_store',
          appVersion: packageInfo.version,
          osVersion: iosDeviceInfo.systemVersion,
          fcmToken: '',
          apnsToken: '',
          deviceModel: iosDeviceInfo.model,
          deviceManufacture: 'Apple',
        ),
      )
          .then((value) {
        if (!deviceIdIsExists()) {
          storageBox.write('device_id', deviceId);
        }
      });
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
