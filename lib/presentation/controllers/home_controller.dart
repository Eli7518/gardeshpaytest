import 'package:flutter_gardesh_pay_test/presentation/controllers/login_register_controller.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController {
  late String deviceId;
  late String userEmail;
  late int count;
  late String accessToken;
  late String refreshToken;
  final storageBox = GetStorage();
  final loginRegisterController = Get.find<LoginRegisterController>();

  @override
  void onInit() {
    super.onInit();
    if (storageBox.hasData('count')) {
      int countNumber = storageBox.read('count');
      print(countNumber++);
      storageBox.write('count', countNumber++);
    } else {
      storageBox.write('count', 1);
    }
    print(storageBox.read('count'));
    deviceId = storageBox.read('device_id');
    userEmail = storageBox.read('user_email');
    count = storageBox.read('count');
    accessToken = storageBox.read('access_token');
    refreshToken = storageBox.read('refresh_token');
  }

  void logout() {
    storageBox.remove('count');
    storageBox.remove('user_email');
    storageBox.remove('access_token');
    storageBox.remove('refresh_token');
    loginRegisterController.changePageViewCurrentPage(0);
    Get.rootDelegate.offAndToNamed('/loginRegister');
  }

  @override
  void onReady() {
    super.onReady();
  }
}
