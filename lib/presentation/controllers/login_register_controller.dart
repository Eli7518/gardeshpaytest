import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_gardesh_pay_test/domain/adapters/login_register_repository_adapter.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/email_otp_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/request/login_register_req.dart';
import 'package:flutter_gardesh_pay_test/domain/entity/response/register_api_res.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginRegisterController extends SuperController<RegisterApiRes> {
  LoginRegisterController({required this.loginRegisterRepository});

  final ILoginRegisterRepository loginRegisterRepository;
  final storageBox = GetStorage();
  String deviceId = "";
  PageController pageViewController = PageController();
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  TextEditingController emailLoginController = TextEditingController();
  TextEditingController passwordLoginController = TextEditingController();
  final loginIsLoading = false.obs;

  final GlobalKey<FormState> registerFormKey = GlobalKey<FormState>();
  TextEditingController emailRegisterController = TextEditingController();
  TextEditingController passwordRegisterController = TextEditingController();
  final registerIsLoading = false.obs;

  final GlobalKey<FormState> verifyFormKey = GlobalKey<FormState>();
  TextEditingController verifyCodeController = TextEditingController();
  final verifyIsLoading = false.obs;

  String? emailTextFieldValidator(String? value) {
    if (value == "")
      return 'Required!';
    else if (value != "" &&
        !RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
            .hasMatch(value!))
      return 'Invalid Email Address!';
    else
      return null;
  }

  String? passwordTextFieldValidator(String? value) {
    if (value == "")
      return 'Required!';
    else
      return null;
  }

  Future<void> sendLoginInfo() async {
    if (loginFormKey.currentState!.validate()) {
      loginIsLoading.toggle();
      final user = await loginRegisterRepository.loginUser(
        deviceId,
        LoginRegisterReq(
          email: emailLoginController.text,
          password: passwordLoginController.text,
        ),
      );
      loginIsLoading.toggle();
      if (user.status.isSuccess) {
        storageBox.write('user_email', emailLoginController.text);
        storageBox.write('access_token', user.result!.data!.accessToken);
        storageBox.write('refresh_token', user.result!.data!.refreshToken);
        Get.rootDelegate.offAndToNamed('/home');
      } else {
        changePageViewCurrentPage(0);
        Get.snackbar('Error!', 'Incorrect User Info!\nTry again please...',
            backgroundColor: Colors.red);
      }
    }
  }

  Future<void> sendRegisterInfo() async {
    if (registerFormKey.currentState!.validate()) {
      registerIsLoading.toggle();
      final user = await loginRegisterRepository.registerUser(
        deviceId,
        LoginRegisterReq(
          email: emailRegisterController.text,
          password: passwordRegisterController.text,
        ),
      );
      registerIsLoading.toggle();
      if (user.status.isSuccess) {
        storageBox.write('user_email', emailRegisterController.text);
        changePageViewCurrentPage(3);
      } else {
        changePageViewCurrentPage(0);
        Get.snackbar('Error!', 'Incorrect Data!\nTry again please...',
            backgroundColor: Colors.red);
      }
    }
  }

  Future<void> sendVerifyCode() async {
    if (verifyFormKey.currentState!.validate()) {
      verifyIsLoading.toggle();
      print(emailRegisterController.text);
      print(verifyCodeController.text);
      final user = await loginRegisterRepository.verifyCode(
        deviceId,
        VerifyCodeReq(
          email: emailRegisterController.text,
          otpCode: verifyCodeController.text,
        ),
      );
      verifyIsLoading.toggle();
      if (user.status.isSuccess) {
        storageBox.write('access_token', user.result!.data!.accessToken);
        storageBox.write('refresh_token', user.result!.data!.refreshToken);
        Get.rootDelegate.offAndToNamed('/home');
      } else {
        changePageViewCurrentPage(0);
        Get.snackbar('Error!', 'Incorrect Verify Code!\nTry again please...',
            backgroundColor: Colors.red);
      }
    }
  }

  void changePageViewCurrentPage(int index) {
    switch (index) {
      case 0:
        emailLoginController.clear();
        emailRegisterController.clear();
        passwordLoginController.clear();
        passwordRegisterController.clear();
        verifyCodeController.clear();
        pageViewController.jumpToPage(index);
        break;
      case 1:
        pageViewController.jumpToPage(index);
        break;
      case 2:
        pageViewController.jumpToPage(index);
        break;
      case 3:
        pageViewController.jumpToPage(index);
        break;
      default:
        pageViewController.jumpToPage(0);
        break;
    }
  }

  Future<bool> onWillPop() async {
    if (pageViewController.page != 0) {
      changePageViewCurrentPage(0);
      return false;
    } else
      return true;
  }

  @override
  void onInit() async {
    super.onInit();
    deviceId = storageBox.read('device_id');
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
